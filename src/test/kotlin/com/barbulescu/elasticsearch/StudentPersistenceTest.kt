package com.barbulescu.elasticsearch

import com.barbulescu.elasticsearch.Gender.MALE
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.Duration

@IntegrationTest
class StudentPersistenceTest(val repository: StudentRepository) {

    @Test
    fun `simple student persistence`() {
        val initial = Student(12, "Vasile", 21, StudentDetails("none"), MALE)

        val actual: Student? = repository.save(initial)
            .flatMap { repository.findById("12") }
            .block(Duration.ofMillis(500))

        assertEquals(initial, actual)
    }
}