package com.barbulescu.elasticsearch

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestConstructor.AutowireMode.ALL

@SpringBootTest
@ExtendWith(ElasticsearchCleanerExtension::class)
@TestConstructor(autowireMode = ALL)
annotation class IntegrationTest
