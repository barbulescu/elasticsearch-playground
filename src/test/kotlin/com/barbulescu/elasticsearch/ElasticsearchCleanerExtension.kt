package com.barbulescu.elasticsearch

import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ExtensionContext.Namespace
import org.springframework.context.support.GenericApplicationContext
import org.springframework.stereotype.Component
import org.springframework.test.context.TestContextManager
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.Duration

class ElasticsearchCleanerExtension : BeforeEachCallback {

    override fun beforeEach(context: ExtensionContext) {
        val contextManager = testContextManager(context)
        val applicationContext = contextManager.testContext.applicationContext as GenericApplicationContext
        val cleaner = applicationContext.getBean(ElasticsearchCleaner::class.java)
        cleaner.cleanup()
    }

    private fun testContextManager(context: ExtensionContext): TestContextManager {
        val namespace = Namespace.create(SpringExtension::class.java)
        val store = context.getStore(namespace)
        return store.getOrComputeIfAbsent(context.requiredTestClass, ::TestContextManager, TestContextManager::class.java)
    }
}

@Component
class ElasticsearchCleaner(val studentRepository: StudentRepository) {
    fun cleanup() {
        studentRepository.deleteAll()
            .block(Duration.ofSeconds(2))
    }
}