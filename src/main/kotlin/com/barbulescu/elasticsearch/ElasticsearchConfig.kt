package com.barbulescu.elasticsearch

import com.barbulescu.elasticsearch.Gender.OTHER
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.core.convert.support.GenericConversionService
import org.springframework.data.convert.ReadingConverter
import org.springframework.data.convert.WritingConverter
import org.springframework.data.elasticsearch.client.ClientConfiguration
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient
import org.springframework.data.elasticsearch.client.reactive.ReactiveRestClients
import org.springframework.data.elasticsearch.config.AbstractReactiveElasticsearchConfiguration
import org.springframework.data.elasticsearch.core.ElasticsearchEntityMapper
import org.springframework.data.elasticsearch.core.EntityMapper
import org.springframework.data.elasticsearch.core.convert.ElasticsearchCustomConversions


@Configuration
class ElasticsearchConfig(private val conversionService: GenericConversionService) : AbstractReactiveElasticsearchConfiguration() {

    override fun reactiveElasticsearchClient(): ReactiveElasticsearchClient {
        return ReactiveRestClients.create(ClientConfiguration.localhost())
    }

    @Bean
    override fun entityMapper(): EntityMapper {
        val entityMapper = ElasticsearchEntityMapper(elasticsearchMappingContext(), conversionService)
        val converters = listOf(
            ElasticsearchValueToStringConverter(),
            StringToGenderConverter()
        )
        entityMapper.setConversions(ElasticsearchCustomConversions(converters))
        return entityMapper
    }
}

@ReadingConverter
class StringToGenderConverter : Converter<String, Gender> {
    private val mapping: Map<String, Gender> = Gender.values().associateBy { it.esValue() }

    override fun convert(source: String): Gender {
        return mapping.getOrDefault(source, OTHER)
    }
}

@WritingConverter
class ElasticsearchValueToStringConverter : Converter<ElasticsearchValue, String> {

    override fun convert(source: ElasticsearchValue): String {
        return source.esValue()
    }
}
