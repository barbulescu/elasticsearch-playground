package com.barbulescu.elasticsearch

import com.barbulescu.elasticsearch.Gender.MALE
import com.barbulescu.elasticsearch.StreamExtractor.FluxRequest
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.binder.jvm.ExecutorServiceMetrics
import org.elasticsearch.index.query.QueryBuilders.matchAllQuery
import org.reactivestreams.Subscription
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import reactor.core.publisher.BaseSubscriber
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import reactor.core.scheduler.Schedulers.fromExecutor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors.newFixedThreadPool
import java.util.concurrent.TimeUnit.MINUTES
import kotlin.system.exitProcess


@Component
class StartUp(
    private val studentRepository: StudentRepository,
    private val streamExtractor: StreamExtractor,
    private val registry: MeterRegistry,
    private val meterReporter: MeterReporter
) {

    private val executor: ExecutorService = ExecutorServiceMetrics.monitor(registry, newFixedThreadPool(10), "abc")

    @EventListener
    fun startUp(event: ApplicationReadyEvent) {
//        initialize()

//        process2()
        process1()

        Thread.sleep(15_000)

        meterReporter.logMetrics()

        executor.awaitTermination(10, MINUTES)
        executor.shutdown()
        println("stop")
        exitProcess(0)
    }

    private fun process1() {
        val request = FluxRequest(
            index = "student",
            batchSize = 1000,
            query = matchAllQuery(),
            type = Student::class.java,
            tieBreakField = "id",
            keyExtractor = Student::tiebreak
        )
        streamExtractor.flux(request)
            .metrics()
            .limitRate(2)
            .parallel(10)
            .runOn(fromExecutor(executor), 2)
            .flatMap { processBatch(it) }
            .subscribe()
    }

    private fun processBatch(it: List<Student>): Flux<Student> {
        println("processing on thread ${Thread.currentThread()} ${it.first().id} -> ${it.last().id}")
        Thread.sleep(1_000)
        return Flux.fromIterable(it)
    }

    class SampleSubscriber<T> : BaseSubscriber<T>() {
        override fun hookOnSubscribe(subscription: Subscription) {
            println("Subscribed")
            request(1)
        }

        public override fun hookOnNext(value: T) {
//            println(value)
            request(1)
        }
    }

    private fun process2() {
        val start = System.currentTimeMillis()
        studentRepository.findAll()
            .onBackpressureBuffer(200)
            .log()
            .buffer(10)
            .parallel(2)
            .runOn(Schedulers.parallel())
            .flatMap { processBuffer1(it) }
            .sequential()
            .buffer(10)
            .parallel(2)
            .runOn(Schedulers.parallel())
            .flatMap { processBuffer2(it) }
            .sequential()
            .buffer(10)
            .parallel(2)
            .runOn(Schedulers.parallel())
            .doAfterTerminate { println("Taken ${System.currentTimeMillis() - start}") }
            .subscribe { println("done ${it.map { it.id }.joinToString(",")} on thread ${Thread.currentThread()}") }
    }

    private fun processBuffer1(buffer: List<Student>): Flux<Student> {
        println("start processing 1 on thread ${Thread.currentThread()}")
        Thread.sleep(10_000)
        val a = Flux.fromIterable(buffer
            .filter { it.id?.toInt()?.rem(2) == 0 })
        println("stop processing 1 on thread ${Thread.currentThread()}")
        return a

    }

    private fun processBuffer2(buffer: List<Student>): Flux<Student> {
        println("start processing 2 on thread ${Thread.currentThread()}")
        Thread.sleep(10_000)
        val a = Flux.fromIterable(buffer)
        println("stop processing 2 on thread ${Thread.currentThread()}")
        return a

    }

    private fun initialize() {
        println()
        println("initialize")
        println()

        studentRepository.deleteAll()
            .block()

        val count = Flux.range(1, 30_000)
            .map(this::save)
            .blockLast()

        println("saved $count")
    }

    private fun saveBatch(batch: MutableList<Int>) =
        studentRepository.saveAll(batch.map { id -> Student(id.toLong(), "Marius-$id", 23, StudentDetails("none"), MALE) })

    private fun save(id: Int): Student? =
        studentRepository.save(Student(id.toLong(), "Marius-$id", 23, StudentDetails("none"), MALE)).block()
}