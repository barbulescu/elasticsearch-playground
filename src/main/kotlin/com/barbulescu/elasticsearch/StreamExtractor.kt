package com.barbulescu.elasticsearch

import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RequestOptions.DEFAULT
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.index.query.QueryBuilder
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.elasticsearch.search.builder.SearchSourceBuilder.searchSource
import org.springframework.data.elasticsearch.core.EntityMapper
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux

@Component
class StreamExtractor(private val client: RestHighLevelClient, private val mapper: EntityMapper) {

    fun <T> flux(request: FluxRequest<T>): Flux<List<T>> {
        return Flux.generate<List<T>, String?>({ null }) { currentState, sink ->
            println("sending request on thread ${Thread.currentThread()} $currentState")
                val result = execute(currentState, request)

                if (result.isEmpty()) {
                    sink.complete()
                return@generate currentState
                }
                sink.next(result)
            return@generate request.keyExtractor.invoke(result.last())
        }
    }

    private fun <T> execute(start: String?, request: FluxRequest<T>): List<T> {

        val searchRequest = SearchRequest(request.index).apply {
            source(request.searchSource(start))
        }


        return client.search(searchRequest, DEFAULT).hits.hits
            .map { it.sourceAsString }
            .map { mapper.mapToObject(it, request.type) }
    }

    data class FluxRequest<T>(
        val index: String,
        val batchSize: Int = 3,
        val query: QueryBuilder,
        val type: Class<T>,
        val tieBreakField: String,
        val keyExtractor: (T) -> String
    ) {

        fun searchSource(start: String?): SearchSourceBuilder {
            val builder = searchSource()
                .sort(tieBreakField)
                .size(batchSize)
                .query(query)

            if (start != null) {
                builder.searchAfter(arrayOf(start))
            }
            return builder
        }
    }
}