package com.barbulescu.elasticsearch

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ReindexController(private val service: ReindexService) {

    @GetMapping("/reindex")
    fun reindex(): String {
        service.reindex()
        return "Done"
    }
}