package com.barbulescu.elasticsearch

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository
import org.springframework.stereotype.Repository

@Document(indexName = "student", type = "_doc")
data class Student(
    @Id val id: Long?,
    val name: String,
    val age: Int,
    val details: StudentDetails,
    val gender: Gender) {

    @Transient
    val tiebreak = id.toString()
}

data class StudentDetails(val value: String)

@Repository
interface StudentRepository : ReactiveElasticsearchRepository<Student, String>

@Document(indexName = "student2", type = "_doc")
data class Student2(@Id val id: Long?, val name: String, val age: Int, val details: String)

@Repository
interface Student2Repository : ReactiveElasticsearchRepository<Student2, String>


@Document(indexName = "details", type = "_doc")
data class Details(@Id val id: Long?, val value: String)

@Repository
interface DetailsRepository : ReactiveElasticsearchRepository<Details, String>

enum class Gender(private val esValue: String) : ElasticsearchValue {
    MALE("m"),
    FEMALE("f"),
    OTHER("o");

    override fun esValue(): String = esValue
}

interface ElasticsearchValue {
    fun esValue() : String
}

