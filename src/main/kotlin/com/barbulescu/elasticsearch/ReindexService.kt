package com.barbulescu.elasticsearch

import com.barbulescu.elasticsearch.Gender.MALE
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest
import org.elasticsearch.client.Request
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.stereotype.Component

@Component
class ReindexService(
    private val studentRepository: StudentRepository,
    private val student2Repository: Student2Repository,
    private val detailsRepository: DetailsRepository,
    private val client: RestHighLevelClient
) {

    fun reindex() {
        client.indices().delete(DeleteIndexRequest("student", "student2", "details"), RequestOptions.DEFAULT)

        studentRepository.save(Student(1, "Vasile", 15, StudentDetails("a1"), MALE)).log().subscribe()
        studentRepository.save(Student(2, "Mihai", 16, StudentDetails("a2"), MALE)).log().subscribe()
        studentRepository.save(Student(3, "Marius", 12, StudentDetails("a3"), MALE)).log().subscribe()
        student2Repository.save(Student2(34, "abc", 22, "a4")).log().subscribe()
        detailsRepository.save(Details(2, "x1")).log().subscribe()

        reindex(
            """
                    |{
                    |  "source": {
                    |    "index": "student",
                    |    "_source": ["name", "age", "details.value"]
                    |  },
                    |  "dest": {
                    |    "index": "student2"
                    |  },
                    |  "script": {
                    |    "source": "ctx._source.details=ctx._source.details.value;",
                    |    "lang": "painless"
                    |  }
                    |}""".trimMargin()
        )
        reindex(
            """
                    |{
                    |  "conflicts": "proceed",
                    |  "source": {
                    |    "index": "details",
                    |    "_source": ["value"]
                    |  },
                    |  "dest": {
                    |    "index": "student2"
                    |  },
                    |  "script": {
                    |    "source": "ctx._source.details=ctx._source.value;ctx._source.remove('value')",
                    |    "lang": "painless"
                    |  }
                    |}""".trimMargin()
        )
    }

    private fun reindex(value: String) {
        val request = Request("POST", "_reindex")
        request.setJsonEntity(value)
        client.lowLevelClient.performRequest(request)
    }
}