package com.barbulescu.elasticsearch

import io.micrometer.core.instrument.Meter
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.stereotype.Component

@Component
class MeterReporter(private val registry: MeterRegistry) {

    fun logMetrics() {
        println("----------------")
        registry.meters
            .sortedBy { it.id.name }
            .forEach { meter: Meter ->
                val id = meter.id

                val tags = id.tags.joinToString(separator = ", ") { "${it.key}=${it.value}" }
                val key = "${id.name}[${tags}] - ${id.baseUnit}: "

                val values = meter.measure().joinToString(", ") { it.toString() }

                println("$key -> $values")
            }
        println("----------------")
    }

}